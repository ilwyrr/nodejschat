var http = require('http');
var fs = require('fs');
var ent = require('ent');

var server = http.createServer(function(req, res){
	fs.readFile('./index.html', 'utf-8', function(error, content){
		res.writeHead(200, {"Content-Type": "text/html"});
		res.end(content);
	});
});

var io = require('socket.io').listen(server);

io.sockets.on('connection', function(socket, pseudo){

	socket.on('newUser', function(pseudo){
		socket.pseudo = ent.encode(pseudo);
		console.log(socket.pseudo + ' is connected')
		socket.broadcast.emit('newUser', pseudo);
	});
	
	socket.on('message', function(message){
		message = ent.encode(message);
		console.log(socket.pseudo + ' is saying ' + message);
		socket.broadcast.emit('message', {pseudo: socket.pseudo, message: message});
	});
	
});



server.listen(8080);